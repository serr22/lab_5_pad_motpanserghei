package dvl.srg.pnodecore;


import dvl.srg.helpers.GetEmployee;
import dvl.srg.infonode.request.HttpPutRequest;
import dvl.srg.pservice.UpdateDatesService;
import org.apache.http.HttpException;

import java.io.IOException;

public class App
{
    private static final String URL = "http://localhost:8080/employee/";

    public static void main( String[] args ) throws IOException, HttpException, InterruptedException {

        if (args.length != 1) {
            System.out.println("[ERROR] Specificare [args] port [10001-10004] !");
            return;
        }

        int port = Integer.parseInt(args[0]);

        System.out.println("[INFO] -------------------------------");
        System.out.println("[INFO] START INFONODE " + port + " ...");

        Thread.sleep(2000);
        System.out.println("[INFO] -------------------------------");
        System.out.println("[INFO] Local data: " + GetEmployee.getEmployee(port));
        System.out.println("[INFO] -------------------------------");
        System.out.println("[INFO] Send PUT Request to DW ... ");
        System.out.println("[INFO] -------------------------------");
        new HttpPutRequest().sendPUTRequest(URL, GetEmployee.getEmployee(port));

        new UpdateDatesService(GetEmployee.getEmployee(port)).start();

    }
}


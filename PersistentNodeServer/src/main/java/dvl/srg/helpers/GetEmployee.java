package dvl.srg.helpers;

import dvl.srg.infonode.nodeinfoservice.model.Employee;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by administrator on 12/20/15.
 */
public class GetEmployee {

    public static Employee getEmployee(int id) {

        Employee employee = null;

        try {
            FileInputStream file = new FileInputStream(new File(GetEmployee.class.getResource("/employee.xml").getFile()));
            DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = builderFactory.newDocumentBuilder();
            Document xmlDocument = builder.parse(file);
            XPath xPath = XPathFactory.newInstance().newXPath();

            String expression = "/employees/employee[@id='"+ id + "']";

            Node node = (Node) xPath.compile(expression).evaluate(xmlDocument, XPathConstants.NODE);
            if(null != node) {
                Element eElement = (Element) node;
                employee = new Employee(Integer.parseInt(eElement.getAttribute("id")));
                employee.setFirstName(eElement.getElementsByTagName("firstName").item(0).getTextContent());
                employee.setLastName(eElement.getElementsByTagName("lastName").item(0).getTextContent());
                employee.setDepartment(eElement.getElementsByTagName("department").item(0).getTextContent());
                employee.setSalary(Double.valueOf(eElement.getElementsByTagName("salary").item(0).getTextContent()));
            }

        } catch ( ParserConfigurationException | SAXException | IOException | XPathExpressionException e) {
            e.printStackTrace();
        }
        return employee;

    }
}

package dvl.srg.infonode.nodehttpservice;

import dvl.srg.infonode.nodeinfoservice.InfoService;
import dvl.srg.infonode.nodeinfoservice.LocationService;
import org.apache.http.ConnectionClosedException;
import org.apache.http.HttpException;
import org.apache.http.HttpServerConnection;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpService;

import java.io.IOException;

/**
 * Created by administrator on 12/20/15.
 */
public class Worker extends Thread {

    private final HttpService httpservice;
    private final HttpServerConnection conn;
    private final InfoService infoService;
    private final LocationService locationService;
    private final InfoService updateInfoService;

    public Worker(
            final HttpService httpservice,
            final HttpServerConnection conn,
            final InfoService infoService,
            final LocationService locationService,
            final InfoService updateInfoService
            ) {
        super();
        this.httpservice = httpservice;
        this.conn = conn;
        this.infoService = infoService;
        this.locationService = locationService;
        this.updateInfoService = updateInfoService;
        this.setDaemon(true);
    }

    @Override
    public void run() {
        System.out.println("[INFO]  New connection thread");

        HttpContext context = new BasicHttpContext(null);
        context.setAttribute("infoService", infoService);
        context.setAttribute("locationService", locationService);
        context.setAttribute("updateInfoService", updateInfoService);

        try {
            while (!Thread.interrupted() && this.conn.isOpen()) {
                this.httpservice.handleRequest(this.conn, context);
            }
        } catch (ConnectionClosedException ex) {
            System.err.println("[ERROR]  Client closed connection");

        } catch (IOException ex) {
            System.err.println("[ERROR]  I/O error: " + ex.getMessage());

        } catch (HttpException ex) {
            System.err.println("[ERROR]  Unrecoverable HTTP protocol violation: " + ex.getMessage());

        } finally {
            try {
                this.conn.shutdown();
            } catch (IOException ignore) {
            }
        }
    }
}

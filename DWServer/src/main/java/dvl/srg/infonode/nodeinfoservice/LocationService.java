package dvl.srg.infonode.nodeinfoservice;

import dvl.srg.infonode.nodeinfoservice.model.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 12/20/15.
 */
public class LocationService {

    private List<Location> locations;

    public LocationService () {
        locations = new ArrayList<>();
        locations.add(new Location("127.0.0.1", 10001, 1));
        locations.add(new Location("127.0.0.1", 10002, 2));
        locations.add(new Location("127.0.0.1", 10003, 3));
        locations.add(new Location("127.0.0.1", 10004, 4));
    }

    public LocationService (List<Location> locations) {
        this.locations = locations;
    }

    public void addNodeLocation(Location location) {
        locations.add(location);
    }

    public List<Location> getLocations () {
        return locations;
    }

    public Location getLocation(int index) {
        return locations.get(index);
    }

}

package dvl.srg.infonode.nodeinfoservice.model;

import java.io.Serializable;

/**
 * Created by administrator on 12/20/15.
 */
public class Location implements Serializable{

    private static final long serialVersionUID = 5975468863982872614L;

    private String host;
    private int port;
    private int nodeNr;

    public Location() {
    }

    public Location(String host, int port, int nodeNr) {
        this.host = host;
        this.port = port;
        this.nodeNr = nodeNr;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getNodeNr() {
        return nodeNr;
    }

    public void setNodeNr(int nodeNr) {
        this.nodeNr = nodeNr;
    }

    @Override
    public String toString() {
        return "Location{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", nodeNr=" + nodeNr +
                '}';
    }
}

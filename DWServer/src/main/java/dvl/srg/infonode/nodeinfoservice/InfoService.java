package dvl.srg.infonode.nodeinfoservice;

import dvl.srg.infonode.nodeinfoservice.model.Employee;

import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Created by administrator on 12/20/15.
 */
public class InfoService {

    private CopyOnWriteArraySet<Employee> employees = new CopyOnWriteArraySet<Employee>();

    public CopyOnWriteArraySet<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(CopyOnWriteArraySet<Employee> employees) {
        this.employees = employees;
    }

    public boolean add(Employee e) {
        return employees.add(e);
    }

    public boolean remove(Employee e) {
        return employees.remove(e);
    }

    public Object[] toArray() {
        return employees.toArray();
    }
}
